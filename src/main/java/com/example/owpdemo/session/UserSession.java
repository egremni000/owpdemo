package com.example.owpdemo.session;

import com.example.owpdemo.model.Role;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static org.springframework.web.context.WebApplicationContext.SCOPE_SESSION;

@Component
@Scope(scopeName = SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Getter
@Setter
public class UserSession {
    private Optional<String> username = Optional.empty();
    private Optional<Role> role = Optional.empty();
}
