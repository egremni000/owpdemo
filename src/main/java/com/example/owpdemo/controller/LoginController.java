package com.example.owpdemo.controller;

import com.example.owpdemo.service.LoginService;
import com.example.owpdemo.session.UserSession;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@RequestMapping("/login")
@Controller
public class LoginController {
    private final LoginService loginService;
    private final UserSession userSession;
    private final HttpSession httpSession;

    public LoginController(LoginService loginService, UserSession userSession, HttpSession httpSession) {
        this.loginService = loginService;
        this.userSession = userSession;
        this.httpSession = httpSession;
    }

    @GetMapping
    public Object doGetLogin(@RequestParam(value = "error", required = false) String error) {
        if (this.userSession.getUsername().isPresent()) {
            return "redirect:/";
        }
        //language=HTML
        var page = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <link rel="stylesheet" href="css/index.css">
                    <meta charset="utf-8">
                    <title>Index page</title>
                </head>
                <body>
                 <h1>Login here</h1>
                 <form class='flex-column' method='POST'>
                    <label for='login-input'>Username:</label>
                    <input type='text' id='login-input' name='username'>
                    <label for='login-password'>Password:</label>
                    <input type='password' id='login-password' name='password'>
                    <button type='submit' class='mt ml mb mr'>Login</button>
                 </div>
                 {{anyErrorElement}}
                </body>
                </html>
                """;
        if (error != null) {
            page = page.replace("{{anyErrorElement}}",
                    //language=HTML
                    """
                            <h2>Neispravan username ili password!</h2>
                            """
            );
        } else {
            page = page.replace("{{anyErrorElement}}", "");
        }
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @PostMapping
    public Object doPostLogin(@ModelAttribute("username") String username, @ModelAttribute("password") String password) {
        if (username.isEmpty() || password.isEmpty()) {
            return "redirect:/login?error";
        }
        var user = this.loginService.findUserByUsername(username);
        if (user.isEmpty()) {
            return "redirect:/login?error";
        }

        var userGet = user.get();
        if (!userGet.getPassword().equals(password)) {
            return "redirect:/login?error";
        }

        this.userSession.setUsername(Optional.of(userGet.getUsername()));
        this.userSession.setRole(Optional.of(userGet.getRole()));

        return "redirect:/";
    }




    @GetMapping("/logout")
    public Object logOut() {
        this.httpSession.invalidate();
        return "redirect:/";
    }


}
