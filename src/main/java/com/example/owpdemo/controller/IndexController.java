package com.example.owpdemo.controller;

import com.example.owpdemo.model.Role;
import com.example.owpdemo.model.Vaccine;
import com.example.owpdemo.repository.VaccineRepository;
import com.example.owpdemo.session.UserSession;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Controller
@RequestMapping()
public class IndexController {
    private final UserSession session;
    private final VaccineRepository vaccineRepository;

    public IndexController(UserSession session, VaccineRepository vaccineRepository) {
        this.session = session;
        this.vaccineRepository = vaccineRepository;
    }

    @ResponseBody
    @GetMapping
    public String index() {
        //language=HTML

        String adminVaccines = "";
        for (Vaccine vaccine : vaccineRepository.findAll()) {
            var date = vaccine.getLastShipmentTime();
            var stringDate = " ";
            if (date != null) {
                stringDate = date.toString().replace("T", " ");
            }

            adminVaccines += """
                    
                    <tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%d</td>
                    
                    <td><a href="/vaccine/order/%s" class='button'>Order vaccine</a></td>
                    </td>
                    </tr>
                
                    """.formatted(vaccine.getNameOfVaccine(),
                    vaccine.getManufacturer().getName(),
                    vaccine.getManufacturer().getCountry(),
                    stringDate,
                    vaccine.getAvailableQuantity(),
                    vaccine.getId());
        }

        String nurseVaccines = "";
        for (Vaccine vaccine : vaccineRepository.findAll()) {
            var date = vaccine.getLastShipmentTime();
            var stringDate = " ";
            if (date != null) {
                stringDate = date.toString().replace("T", " ");
            }
            //language=HTML
            nurseVaccines += """
                    <style>a.button {
                               -webkit-appearance: button;
                               -moz-appearance: button;
                               appearance: button;
                           
                               text-decoration: none;
                               color: initial;
                           }</style>
                     <form>     
                    <tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%d</td>
                    
                    <td><a href="/vaccine/give/%s" class=button>Give vaccine</a></td>
                    
                    </td>
                    </tr>
                    </form> 
                    """.formatted(vaccine.getNameOfVaccine(),
                    vaccine.getManufacturer().getName(),
                    vaccine.getManufacturer().getCountry(),
                    stringDate,
                    vaccine.getAvailableQuantity(),
                    vaccine.getId());

        }
        //language=HTML
        String adminTable = """
                <table>
                <thead>
                <tr>
                <th>Vaccine Name</th>
                <th>Manufacturer</th>
                <th>Country</th>
                <th>Last shipment</th>
                <th>Available quantity</th>
                </tr></thead>
                <tbody>
                %s
                </tbody>
                </table>
                """.formatted(adminVaccines);
        //language=HTML
        String nurseTable = """
                <table>
                <thead>
                <tr>
                <th>Vaccine Name</th>
                <th>Manufacturer</th>
                <th>Country</th>
                <th>Last shipment</th>
                <th>Available quantity</th>
                </tr></thead>
                <tbody>
                %s
                </tbody>
                </table>
                """.formatted(nurseVaccines);
        //language=HTML
        var page = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <style>
                    a.button {
                       -webkit-appearance: button;
                       -moz-appearance: button;
                       appearance: button;
                   
                       text-decoration: none;
                       color: initial;
                           }
                    #button1 {
                    margin: 5px;
                    
                    }
                    .container-1 table,th,td{
                        border: 1px solid black;
                    }
                    
                    th,td {
                        padding: 15px;
                        text-align: left;
                    }
                    </style>
                    <meta charset="utf-8">
                    <title>Index page</title>
                </head>
                <body>
                 <h1>Dobrodosli u Rodic kovid centar!</h1>
                 <div>
                 {{navigateTemplate}}
                 </div>
                </body>
                </html>
                """;
        //language=HTML
        String template = "";
        if (this.session.getRole().isEmpty()) {
            template = """
                    <a href='/login'>Login here</a>
                       """;
        } else if (this.session.getRole().get().equals(Role.ADMIN)) {
            template = """
                    <a href='/profile'>Moj profil</a>
                    <a href='/login/logout'>Log out</a>
                    <a href='/vaccine' id='button1'>Add vaccine</a>
                    <div class="container-1">
                        %s
                    </div>
                       """.formatted(adminTable);

        } else if (this.session.getRole().get().equals(Role.NURSE)) {
            template = """
                    <a href='/profile'>Moj profil</a>
                    <a href='/login/logout'>Log out</a>
                    <div class="container-1">
                        %s
                    </div>
                       """.formatted(nurseTable);
        }
        page = page.replace("{{navigateTemplate}}", template);
        return page;
    }

    @GetMapping("/vaccine/give/{id}")
    public Object giveVaccine(@PathVariable("id") String id) {
        if (this.session.getRole().isEmpty()) {
            return "NOT FOUND";
        }
        var vaccines = vaccineRepository.findAll();
        for (Vaccine vac : vaccines) {
            if (vac.getId().equals(id)) {
                if (vac.getAvailableQuantity() != 0) {
                    vac.setAvailableQuantity(vac.getAvailableQuantity() - 1);
                    vaccineRepository.overWriteFile(vaccines);
                }
            }
        }



        return "redirect:/";
    }

    @PostMapping("/vaccine/order/{id}")
    public Object orderVaccine(@PathVariable("id") String id, @RequestParam("availableQuantity") String availableQuantity) {
        if (this.session.getRole().isEmpty()) {
            return "NOT FOUND";
        }
        try {
            if (availableQuantity == null || availableQuantity.equals("") || Integer.parseInt(availableQuantity) < 1) {
                String errorHtml = """
                        <html>
                        <body>
                        Please enter positive number
                        <a href="/vaccine/order/{%s}">BACK</a>
                        </body>
                        </html>
                        """.formatted(id);
                return new ResponseEntity<>(errorHtml, HttpStatus.BAD_REQUEST);
            }
        }catch(Exception e) {
            String errorHtml = """
                        <html>
                        <body>
                        Please enter positive number
                        <a href="/vaccine/order/{%s}">BACK</a>
                        </body>
                        </html>
                        """.formatted(id);
            return new ResponseEntity<>(errorHtml, HttpStatus.BAD_REQUEST);
        }
        var vaccines = vaccineRepository.findAll();
        for (Vaccine vaccine : vaccines) {
            if (vaccine.getId().equals(id)) {
                vaccine.setAvailableQuantity(vaccine.getAvailableQuantity() + Integer.parseInt(availableQuantity));
                vaccine.setLastShipmentTime(LocalDateTime.now());

            }
            vaccineRepository.overWriteFile(vaccines);
        }
        return "redirect:/";
    }

    @ResponseBody
    @GetMapping("/vaccine/order/{id}")
    public Object orderVaccine(@PathVariable("id") String id){
        if (this.session.getRole().isEmpty()) {
            return "NOT FOUND";
        }
        String order = """
                <html>
                <head><h1>Order Vaccine</h1> </head>
                <form action="/vaccine/order/%s" method="post">
                <label for="availableQuantity">Order vaccines </label>
                <input type="text" name="availableQuantity">
                                
                <button type="submit" value="Submit">Add Vaccine</button>
            
                </p>
                """.formatted(id);

        return order;
    }

    }




