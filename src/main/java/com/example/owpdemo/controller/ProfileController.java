package com.example.owpdemo.controller;

import com.example.owpdemo.repository.UserRepository;
import com.example.owpdemo.service.LoginService;
import com.example.owpdemo.session.UserSession;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class ProfileController {
    private final LoginService loginService;
    private final UserSession userSession;
    private final HttpSession httpSession;

    public ProfileController(LoginService loginService, UserSession userSession, HttpSession httpSession) {
        this.loginService = loginService;
        this.userSession = userSession;
        this.httpSession = httpSession;
    }

    @GetMapping("/profile")
    public Object Profile(@RequestParam(value = "error", required = false) String error) {
        if (this.userSession.getRole().isEmpty()) {
            return "NOT FOUND";
        }
        var rlName= this.loginService.findUserByUsername(this.userSession.getUsername().get()).get().getName();
        var userEdit= this.loginService.findUserByUsername(this.userSession.getUsername().get()).get().getUsername();
        //language=HTML
        var page = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <link rel="stylesheet" href="css/index.css">
                    <meta charset="utf-8">
                    <title>Index page</title>
                </head>
                <body>
                 <h1>Profil</h1>
                 <form class='flex-column' method='POST'>
                  <h2>Vase ime je:%s </h2>
                   <a href='/login/logout'>Log out</a>
                 </div>
                 {{anyErrorElement}}
                </body>
                </html>
                """.formatted(rlName,userEdit);
        if (error != null) {
            page = page.replace("{{anyErrorElement}}",
                    //language=HTML
                    """
                            <h2>Doslo je do greske!</h2>
                            """
            );
        } else {
            page = page.replace("{{anyErrorElement}}", "");
        }
        return new ResponseEntity<>(page, HttpStatus.OK);
    }
}
