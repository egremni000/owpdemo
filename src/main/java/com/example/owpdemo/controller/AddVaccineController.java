package com.example.owpdemo.controller;

import com.example.owpdemo.model.Manufacturer;
import com.example.owpdemo.model.Vaccine;
import com.example.owpdemo.repository.ManufacturerRepository;
import com.example.owpdemo.repository.VaccineRepository;
import com.example.owpdemo.session.UserSession;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/vaccine")
public class AddVaccineController {
    private final UserSession session;
    private final VaccineRepository vaccineRepository;
    private final ManufacturerRepository manufacturerRepository;

    public AddVaccineController(UserSession session, VaccineRepository vaccineRepository, ManufacturerRepository manufacturerRepository) {
        this.session = session;
        this.vaccineRepository = vaccineRepository;
        this.manufacturerRepository= manufacturerRepository;
    }
    @GetMapping
    @ResponseBody
    public Object createVaccine() {
        if (this.session.getRole().isEmpty()) {
            return "NOT FOUND";
        }
        List<Manufacturer> manufacturerList = manufacturerRepository.findAll();
        String manufacturer_list = "";
        for (Manufacturer manufacturer : manufacturerList) {
            manufacturer_list+= """
                    <option value="%s">%s(%s)</option>
                    """.formatted(manufacturer.getId(),manufacturer.getName(),manufacturer.getCountry());
        }
        String html = """
                <html>
                <head><h1>Add Vaccine</h1> </head>
                <form action="/vaccine" method="post">
                <label for="nameOfVaccine">Naziv vakcine </label>
                <input type="text" name="nameOfVaccine">
                <p>
                <select name="manufacturer">
                %s
                </select>
                </p>
                <p>
                <button type="submit" value="Submit">Add Vaccine</button>
                </p>
                """.formatted(manufacturer_list);

        return html;
    }
    @PostMapping()
    public Object AddVaccine(@ModelAttribute("nameOfVaccine") String nameOfVaccine, @ModelAttribute("manufacturer") String manufacturer) {
        if (this.session.getRole().isEmpty()) {
            return "NOT FOUND";
        }
        // check for errors, throw error page
        if(nameOfVaccine.isEmpty()){
            String errorHtml =  """
                    <html>
                    <body>
                    Please enter the name of the vaccine
                    <a href="/vaccine">BACK</a>
                    </body>
                    </html>
                    """;
            return new ResponseEntity<>(errorHtml, HttpStatus.BAD_REQUEST);
        }


       else {
            var uuid = UUID.randomUUID().toString();
            Vaccine newVaccine = new Vaccine();
            newVaccine.setId(uuid);

            newVaccine.setNameOfVaccine(nameOfVaccine);
            newVaccine.setAvailableQuantity(0);
            newVaccine.setLastShipmentTime(null);
            newVaccine.setManufacturer(manufacturerRepository.findById(manufacturer));

            vaccineRepository.writeToFile(vaccineRepository.writeToString(newVaccine));
            return "redirect:/";
        }
    }

}
