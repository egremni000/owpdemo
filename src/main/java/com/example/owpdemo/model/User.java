package com.example.owpdemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String username;
    private String password;
    private String name;
    private Role role;

    public String toData() {
        return String.join(";", username, password, name, role.name());
    }
}
