package com.example.owpdemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Vaccine {
    private String id;
    private String nameOfVaccine;
    private Manufacturer manufacturer;
    private LocalDateTime lastShipmentTime;
    private Integer availableQuantity;

}
