package com.example.owpdemo.model;

public enum Role {
    NURSE,
    ADMIN
}
