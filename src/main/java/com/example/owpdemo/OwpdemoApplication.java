package com.example.owpdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OwpdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(OwpdemoApplication.class, args);
    }

}
