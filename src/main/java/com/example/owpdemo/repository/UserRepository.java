package com.example.owpdemo.repository;

import com.example.owpdemo.model.Role;
import com.example.owpdemo.model.User;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {
    private static final String PATH = System.getProperty("user.dir") + "/data/users.txt";

    public Optional<User> findByUsername(String username) {
        return findAll().stream().filter(elem -> elem.getUsername().equals(username)).findFirst();
    }


    private List<User> findAll() {
        ArrayList<User> retList = new ArrayList<>();
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader((PATH)))) {
            while ((line = reader.readLine()) != null) {
                var user = new User();
                String[] split = line.split(";");
                user.setUsername(split[0]);
                user.setPassword(split[1]);
                user.setName(split[2]);
                user.setRole(Role.valueOf(split[3]));
                retList.add(user);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return retList;
    }

    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(PATH, true))) {
            writer.write(user.toData());
            writer.newLine();
        } catch (IOException ignored) {
        }
    }

    private void writeAll(List<User> user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(PATH))) {
            for (User t : user) {
                String s = t.toData();
                writer.write(s);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}