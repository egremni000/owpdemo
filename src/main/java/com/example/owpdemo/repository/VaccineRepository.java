package com.example.owpdemo.repository;

import com.example.owpdemo.model.Manufacturer;
import com.example.owpdemo.model.User;
import com.example.owpdemo.model.Vaccine;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Repository
public class VaccineRepository {
    private final ManufacturerRepository manufacturerRepository;

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private static final String PATH = System.getProperty("user.dir") + "/data/vaccines.txt";

    public VaccineRepository(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @SneakyThrows
    public ArrayList<Vaccine> findAll() {
        val retList = new ArrayList<Vaccine>();
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(PATH))) {
            while ((line = reader.readLine()) != null) {

                String[] split = line.split(";");
                val vaccine = new Vaccine();
                var dateString = split[3];
                LocalDateTime date = null;
                if(!dateString.equals("null"))
                {
                    date= LocalDateTime.parse(dateString,formatter);
                }
                vaccine.setId(String.valueOf(split[0]));
                vaccine.setNameOfVaccine(String.valueOf(split[1]));
                vaccine.setManufacturer(manufacturerRepository.findById(split[2]));
                vaccine.setLastShipmentTime(date);
                vaccine.setAvailableQuantity(Integer.parseInt(split[4]));

                retList.add(vaccine);
            }
        }
        return retList;
    }
    public boolean writeToFile(String vaccine) {
        Path path = Paths.get(PATH);
        try {
            Files.write(path, vaccine.getBytes(), StandardOpenOption.APPEND);
            return true;
        }catch(Exception e) {
            return false;
        }
    }

    public String writeToString(Vaccine vaccine) {
        var date = vaccine.getLastShipmentTime();
        var dateString="null";
        if(date!=null) {
            dateString= date.format(formatter);

        }
            StringJoiner joiner = new StringJoiner(";");
            joiner.add(vaccine.getId())
                    .add(vaccine.getNameOfVaccine())
                    .add(vaccine.getManufacturer().getId())
                    .add(dateString)
                    .add(vaccine.getAvailableQuantity() + "\n");

            return joiner.toString();

    }
    public Vaccine findById(String id) {
        return findAll().stream().filter(elem -> elem.getId().equals(id)).findFirst().orElse(null);
    }


    public boolean overWriteFile(List<Vaccine> vaccines) {
        Path path = Paths.get(PATH);
        String vaccinesString="";
        for(Vaccine vaccine : vaccines) {
            vaccinesString+= writeToString(vaccine);
        }
        try {
            Files.write(path, vaccinesString.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
            return true;
        }catch(Exception e) {
            return false;
        }
    }

//    public void save(Vaccine vaccine) {
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter(PATH, true))) {
//            writer.write(vaccine.toData());
//            writer.newLine();
//        } catch (IOException ignored) {
//        }
//    }
}
