package com.example.owpdemo.repository;

import com.example.owpdemo.model.Manufacturer;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManufacturerRepository {
    private static final String PATH = System.getProperty("user.dir") + "/data/manufacturers.txt";

    public List<Manufacturer> findAll() {
        ArrayList<Manufacturer> retList = new ArrayList<>();
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader((PATH)))) {
            while ((line = reader.readLine()) != null) {
                var man = new Manufacturer();
                String[] split = line.split(";");
                man.setId(split[0]);
                man.setName(split[1]);
                man.setCountry(split[2]);
                retList.add(man);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return retList;
    }

    public Manufacturer findById(String id) {
        return findAll().stream().filter(elem -> elem.getId().equals(id)).findFirst().orElse(null);
    }
}
